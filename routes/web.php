<?php

use Illuminate\Support\Facades\Route;
use App\Models\Link;
use App\Models\Perusahaan;
use App\Http\Controllers\Link\LinkController;
use App\Http\Controllers\Perusahaan\PerusahaanController;
use App\Http\Controllers\GalleryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $perusahaan = Perusahaan::all();
    return view('welcome', compact('perusahaan'));
})->name('home');

Route::get('/link', [LinkController::class, 'index'])->name('link.index'); // laravel 7 keatas
Route::get('/link/create', [LinkController::class, 'create'])->name('link.create');
Route::post('/link/store', [LinkController::class, 'store'])->name('link.store');
Route::get('/link/edit/{link}', [LinkController::class, 'edit'])->name('link.edit');
Route::put('/link/update/{link}', [LinkController::class, 'update'])->name('link.update');
Route::delete('/link/destroy/{link}', [LinkController::class, 'destroy'])->name('link.destroy');

Route::get('/perusahaan', [PerusahaanController::class, 'index'])->name('perusahaan.index'); // laravel 7 keatas
Route::get('/perusahaan/create', [PerusahaanController::class, 'create'])->name('perusahaan.create');
Route::post('/perusahaan/store', [PerusahaanController::class, 'store'])->name('perusahaan.store');
Route::get('/perusahaan/edit/{perusahaan}', [PerusahaanController::class, 'edit'])->name('perusahaan.edit');
Route::put('/perusahaan/update/{perusahaan}', [PerusahaanController::class, 'update'])->name('perusahaan.update');
Route::delete('/perusahaan/destroy/{perusahaan}', [PerusahaanController::class, 'destroy'])->name('perusahaan.destroy');

Route::get('/gallery', [GalleryController::class, 'index'])->name('gallery.index'); // laravel 7 keatas
Route::get('/gallery/create', [GalleryController::class, 'create'])->name('gallery.create');
Route::post('/gallery/store', [GalleryController::class, 'store'])->name('gallery.store');
Route::get('/gallery/edit/{gallery}', [GalleryController::class, 'edit'])->name('gallery.edit');
Route::put('/gallery/update/{gallery}', [GalleryController::class, 'update'])->name('gallery.update');
Route::delete('/gallery/destroy/{gallery}', [GalleryController::class, 'destroy'])->name('gallery.destroy');

Route::get('/home', function () {
    return view('home');
});