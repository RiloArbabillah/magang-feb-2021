<section class="text-gray-600 body-font">
    <div class="container px-5 py-24 mx-auto">
      <div class="text-center mb-20">
        <h1 class="sm:text-3xl text-2xl font-medium title-font text-gray-900 mb-4">Raw Denim Heirloom Man Braid</h1>
        <p class="text-base leading-relaxed xl:w-2/4 lg:w-3/4 mx-auto text-gray-500s">Blue bottle crucifix vinyl post-ironic four dollar toast vegan taxidermy. Gastropub indxgo juice poutine, ramps microdosing banh mi pug.</p>
        <div class="flex mt-6 justify-center">
          <div class="w-16 h-1 rounded-full bg-indigo-500 inline-flex"></div>
        </div>
      </div>
      <div class="flex flex-wrap sm:-m-4 -mx-4 -mb-10 -mt-4 md:space-y-0 space-y-6">
        <div class="p-4 md:w-1/3 flex flex-col text-center items-center">
          <div class="w-20 h-20 inline-flex items-center justify-center rounded-full bg-indigo-100 text-indigo-500 mb-5 flex-shrink-0">
            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-10 h-10" viewBox="0 0 24 24">
              <path d="M22 12h-4l-3 9L9 3l-3 9H2"></path>
            </svg>
          </div>
          <div class="flex-grow">
            <h2 class="text-gray-900 text-lg title-font font-medium mb-3">Shooting Stars</h2>
            <p class="leading-relaxed text-base">Blue bottle crucifix vinyl post-ironic four dollar toast vegan taxidermy. Gastropub indxgo juice poutine, ramps microdosing banh mi pug VHS try-hard.</p>
            <a class="mt-3 text-indigo-500 inline-flex items-center">Learn More
              <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-4 h-4 ml-2" viewBox="0 0 24 24">
                <path d="M5 12h14M12 5l7 7-7 7"></path>
              </svg>
            </a>
          </div>
        </div>
        <div class="p-4 md:w-1/3 flex flex-col text-center items-center">
          <div class="w-20 h-20 inline-flex items-center justify-center rounded-full bg-indigo-100 text-indigo-500 mb-5 flex-shrink-0">
            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-10 h-10" viewBox="0 0 24 24">
              <circle cx="6" cy="6" r="3"></circle>
              <circle cx="6" cy="18" r="3"></circle>
              <path d="M20 4L8.12 15.88M14.47 14.48L20 20M8.12 8.12L12 12"></path>
            </svg>
          </div>
          <div class="flex-grow">
            <h2 class="text-gray-900 text-lg title-font font-medium mb-3">The Catalyzer</h2>
            <p class="leading-relaxed text-base">Blue bottle crucifix vinyl post-ironic four dollar toast vegan taxidermy. Gastropub indxgo juice poutine, ramps microdosing banh mi pug VHS try-hard.</p>
            <a class="mt-3 text-indigo-500 inline-flex items-center">Learn More
              <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-4 h-4 ml-2" viewBox="0 0 24 24">
                <path d="M5 12h14M12 5l7 7-7 7"></path>
              </svg>
            </a>
          </div>
        </div>
        <div class="p-4 md:w-1/3 w-full flex flex-col text-center items-center">
            <div class="flex-grow">
                <h2 class="text-lg title-font font-medium mb-6" style="font-weight: 600; font-size: 17px; color: rgb(246, 255, 255); text-shadow: 0 0 20px rgb(124, 255, 248)">Contact Us</h2>

                <div class="flex-shrink grid lg:grid-cols-2 grid-rows-2 lg:gap-2 gap-3 justify-center text-center content-center mb-6">
                    <div class="flex-shrink">
                        <button class="flex mx-auto text-white bg-indigo-500 border-0 py-2 lg:px-8 px-24 focus:outline-none hover:bg-indigo-600 rounded-full text-lg">
            <a href="#"><i class="fas fa-phone-alt" style="font-size: 1em"></i> Telepon</a>
          </button>
                    </div>
                    <div class="flex-shrink">
                        <button class="flex mx-auto text-white bg-indigo-500 border-0 py-2 lg:px-8 px-24 focus:outline-none hover:bg-indigo-600 rounded-full text-lg">
            <a href="#"><i class="fab fa-whatsapp" style="font-size: 1em"></i> Whatsapp</a>
          </button>
                    </div>
                    <div class="lg:col-span-2">
                        <button class="flex mx-auto text-white bg-indigo-500 border-0 py-2 lg:px-28 px-24 focus:outline-none hover:bg-indigo-600 rounded-full text-lg">
            <a href="#"><i class="far fa-envelope" style="font-size: 1em"></i> Email</a>
          </button>
                    </div>
                </div>

                <h2 class="text-lg title-font font-medium mb-6" style="font-weight: 600; font-size: 17px; color: rgb(246, 255, 255); text-shadow: 0 0 20px rgb(124, 255, 248)">Find Us</h2>
                <ul class="inline-block w-full">
                    <li class="inline-block">
                        <a href="#" class="pointer-cursor">
                            <img src="logo/facebook-icon-circle-seeklogo.png" alt="Facebook - Frematers" class="rounded-full w-10 h-10 border-2 border-transparent hover:border-indigo-400 hover:h-24" />
                        </a>
                    </li>
                    <li class="inline-block">
                        <a href="#" class="pointer-cursor">
                            <img src="logo/pngfind.com-cruz-azul-logo-png-3493616.png" alt="Instagram - Frematers" class="rounded-full w-10 h-10 border-2 border-transparent hover:border-indigo-400 hover:h-24" />
                        </a>
                    </li>
                    <li class="inline-block">
                        <a href="#" class="pointer-cursor">
                            <img src="logo/hiclipart.com.png" alt="YouTube - Frematers" class="rounded-full w-10 h-10 border-2 border-transparent hover:border-indigo-400 hover:h-24" />
                        </a>
                    </li>
                </ul>
            </div>
        </div>
      </div>
      <button class="flex mx-auto mt-16 text-white bg-indigo-500 border-0 py-2 px-8 focus:outline-none hover:bg-indigo-600 rounded text-lg">Button</button>
    </div>
</section>