<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <title>Magang</title>
</head>
<body class="h-screen w-full overflow-hidden overflow-y-auto overflow-x-auto bg-gray-200">
    <div class="bg-blue-500">
        @include('layout.header')
    </div>
    <div>
        @yield('content')
    </div>
    <div>
        @include('layout.footer')
    </div>
</body>
</html>