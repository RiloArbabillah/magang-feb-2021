<div class="container p-4">
    <div class="text-white font-semibold flex-row-reverse flex">
        <a href="{{ route('home') }}" class="px-4 py-2 hover:bg-blue-400 rounded-md tracking-wider leading-4 {{ $path == '/'? 'bg-blue-400' : ''  }}">Home</a>
        <a href="{{ route('link.index') }}" class="px-4 py-2 hover:bg-blue-400 rounded-md tracking-wider leading-4 {{ $path == 'link'? 'bg-blue-400' : ''  }}">Link</a>
        <a href="{{ route('perusahaan.index') }}" class="px-4 py-2 hover:bg-blue-400 rounded-md tracking-wider leading-4 {{ $path == 'perusahaan'? 'bg-blue-400' : '' }}">Perusahaan</a>
    </div>
</div>