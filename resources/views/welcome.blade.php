@extends('layout.default')
@section('content')
    <div class="flex flex-col h-screen bg-gray-300">
        @if (Route::has('login'))
            <div class="top-right links">
                @auth
                    <a href="{{ url('/home') }}">Home</a>
                @else
                    <a href="{{ route('login') }}">Login</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}">Register</a>
                    @endif
                @endauth
            </div>
        @endif

        <div class="flex flex-col align-middle justify-center mt-64">
            <div class="text-7xl flex item-center justify-center mb-10 tracking-wider text-primary">
                Home
            </div>

            <div class="flex flex-col items-center justify-center space-y-5 tracking-wider">
                <table class="divide-y divide-x max-w-md w-full">
                    <thead>
                        <tr class="border border-white">
                            <th width="30" class="tracking-wider px-4 py-1 border-r border-white uppercase text-center">
                                No
                            </th>
                            <th class="tracking-wider px-4 py-1 border-r border-white uppercase text-center">
                                Nama
                            </th>
                            <th class="tracking-wider px-4 py-1 border-r border-white uppercase text-center">
                                Alamat
                            </th>
                            <th class="tracking-wider px-4 py-1 border-r border-white uppercase text-center">
                                Email
                            </th>
                            <th class="tracking-wider px-4 py-1 border-r border-white uppercase text-center">
                                Telpon
                            </th>
                            <th class="tracking-wider px-4 py-1 border-r border-white uppercase text-center">
                                Link
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($perusahaan as $index => $item)
                            <tr class="border border-white">
                                <td class="text-center border-r border-white px-4 py-2">{{ $index+1 }}</td>
                                <td class="text-center border-r border-white px-4 py-2">{{ $item->nama }}</td>
                                <td class="text-center border-r border-white px-4 py-2 whitespace-nowrap">{{ $item->alamat }}</td>
                                <td class="text-center border-r border-white px-4 py-2">{{ $item->email }}</td>
                                <td class="text-center border-r border-white px-4 py-2">{{ $item->telp }}</td>
                                <td class="text-center border-r border-white px-4 py-2 hover:underline">
                                    @if ($item->link)
                                        <a href="{{ $item->link->url }}" target="_blank">{{ $item->link->url }}</a>
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            
            {{-- <div class="text-7xl flex item-center justify-center mb-10 tracking-wider text-primary">
                Laravel
            </div>

            <div class="flex items-center justify-center space-x-10 tracking-wider">
                <a href="https://laravel.com/docs">Docs</a>
                <a href="https://laracasts.com">Laracasts</a>
                <a href="https://laravel-news.com">News</a>
                <a href="https://blog.laravel.com">Blog</a>
                <a href="https://nova.laravel.com">Nova</a>
                <a href="https://forge.laravel.com">Forge</a>
                <a href="https://vapor.laravel.com">Vapor</a>
                <a href="https://github.com/laravel/laravel">GitHub</a>
            </div> --}}
        </div>
    </div>
@endsection
