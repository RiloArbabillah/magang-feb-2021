@extends('layout.default')
@section('content')
    <div class="flex flex-col items-center">
        <div class="flex flex-col align-middle justify-center my-32">
            <div class="text-7xl flex item-center justify-center mb-10 tracking-wider text-blue-500">
                Perusahaan
            </div>

            <div class="flex flex-col items-center justify-center space-y-5 tracking-wider">
                <div class="flex justify-end w-full">
                    <a href="{{ route('perusahaan.create') }}" class="px-4 py-2 bg-green-700 text-center rounded-md text-white hover:bg-green-600">
                        Tambah Perusahaan
                    </a>
                </div>
                <table class="divide-y divide-x max-w-md w-full">
                    <thead>
                        <tr class="border border-white">
                            <th width="30" class="tracking-wider px-4 py-1 border-r border-white uppercase text-center">
                                No
                            </th>
                            <th class="tracking-wider px-4 py-1 border-r border-white uppercase text-center">
                                Nama
                            </th>
                            <th class="tracking-wider px-4 py-1 border-r border-white uppercase text-center">
                                Alamat
                            </th>
                            <th class="tracking-wider px-4 py-1 border-r border-white uppercase text-center">
                                Email
                            </th>
                            <th class="tracking-wider px-4 py-1 border-r border-white uppercase text-center">
                                Telpon
                            </th>
                            <th class="tracking-wider px-4 py-1 uppercase text-center">
                                Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($perusahaan as $index => $item)
                            <tr class="border border-white">
                                <td class="text-center border-r border-white px-4 py-2">{{ $index+1 }}</td>
                                <td class="text-center border-r border-white px-4 py-2">{{ $item->nama }}</td>
                                <td class="text-center border-r border-white px-4 py-2 whitespace-nowrap">{{ $item->alamat }}</td>
                                <td class="text-center border-r border-white px-4 py-2">{{ $item->email }}</td>
                                <td class="text-center border-r border-white px-4 py-2">{{ $item->telp }}</td>
                                {{-- <td class="text-center border-r border-white px-4 py-2">{{ $item->link_id }}</td> --}}
                                <td class="text-center border-r border-white px-4 py-2 flex items-center space-x-3">
                                    <a class="text-blue-500 border border-blue-500 px-4 py-2 rounded-md hover:bg-blue-500 hover:text-white" href="{{ route('perusahaan.edit', $item->id) }}">Edit</a>
                                    <form class="" action="{{ route('perusahaan.destroy', $item->id) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="text-red-500 border border-red-500 px-4 py-2 rounded-md hover:bg-red-500 hover:text-white" type="submit">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
