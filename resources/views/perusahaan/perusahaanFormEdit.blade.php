@extends('layout.default')
@section('content')
    <div class="flex flex-col h-screen bg-gray-300">
        <div class="flex flex-col align-middle justify-center items-center mt-32">
            <div class="text-7xl flex item-center justify-center mb-10 tracking-wider text-primary">
                Edit Perusahaan
            </div>
            <form action="{{ route('perusahaan.update', $perusahaan->id) }}" method="POST" class="max-w-lg space-y-4 w-full border-2 border-blue-500 p-4 rounded-md">
                @csrf
                @method('PUT')
                <div class="w-full">
                    <label for="nama" class="w-full">Nama :</label>
                    <input type="text" name="nama" value="{{ $perusahaan->nama }}" class="w-full px-4 py-2 rounded-md mt-1">
                </div>
                <div class="w-full">
                    <label for="alamat" class="w-full">Alamat :</label>
                    <input type="text" name="alamat" value="{{ $perusahaan->alamat }}" class="w-full px-4 py-2 rounded-md mt-1">
                </div>
                <div class="w-full">
                    <label for="email" class="w-full">Email :</label>
                    <input type="email" name="email" value="{{ $perusahaan->email }}" class="w-full px-4 py-2 rounded-md mt-1">
                </div>
                <div class="w-full">
                    <label for="telp" class="w-full">Telpon :</label>
                    <input type="text" name="telp" value="{{ $perusahaan->telp }}" class="w-full px-4 py-2 rounded-md mt-1">
                </div>
                <div class="w-full">
                    <label for="link" class="w-full">Link :</label>
                    <select name="linkId" id="link" class="w-full px-4 py-2 rounded-md mt-1">
                        <option value="{{ $perusahaan->link_id }}" selected>{{ $perusahaan->link->url }}</option>
                        @foreach ($link as $item)
                            <option value="{{ $item->id }}">{{ $item->url }}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="px-4 py-2 bg-blue-600 text-white hover:bg-blue-500 rounded-md">Simpan</button>
            </form>
        </div>
    </div>
@endsection