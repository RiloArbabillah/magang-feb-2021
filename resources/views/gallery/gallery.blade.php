@extends('layout.default')
@section('content')
    <div class="flex flex-col items-center">
        <div class="flex flex-col align-middle justify-center my-32">
            <div class="text-7xl flex item-center justify-center mb-10 tracking-wider text-blue-500">
                Gallery
            </div>

            <div class="flex flex-col items-center justify-center space-y-5 tracking-wider">
                <div class="flex justify-end w-full">
                    <a href="{{ route('gallery.create') }}" class="px-4 py-2 bg-green-700 text-center rounded-md text-white hover:bg-green-600">
                        Tambah Gallery
                    </a>
                </div>
                <table class="divide-y divide-x max-w-md w-full">
                    <thead>
                        <tr class="border-2 border-white bg-gray-400 text-white">
                            <th width="30" class="tracking-wider px-4 py-1 border-r border-white uppercase text-center">
                                No
                            </th>
                            <th class="tracking-wider px-4 py-1 border-r border-white uppercase text-center">
                                Nama
                            </th>
                            <th class="tracking-wider px-4 py-1 border-r border-white uppercase text-center">
                                path
                            </th>
                            <th class="tracking-wider px-4 py-1 border-r border-white uppercase text-center">
                                meta
                            </th>
                            <th class="tracking-wider px-4 py-1 uppercase text-center">
                                Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($galleries as $index => $item)
                            <tr class="border border-white">
                                <td class="text-center border-r border-white px-4 py-2">{{ $index+1 }}</td>
                                <td class="text-center border-r border-white px-4 py-2">{{ $item->nama }}</td>
                                <td class="text-center border-r border-white px-4 py-2 hover:underline">
                                    <a href="{{ url($item->path) }}">{{ $item->path }}</a>
                                </td>
                                <td class="text-center border-r border-white px-4 py-2">
                                    {{-- <span>
                                        {{ $item->meta->name }}
                                    </span> --}}
                                    <span>
                                        {{ $item->meta->mime }}
                                    </span>
                                    {{-- <span>
                                        {{ $item->meta->type }}
                                    </span> --}}
                                </td>
                                <td class="text-center border-r border-white px-4 py-2 flex items-center space-x-3">
                                    <a class="text-blue-500 border border-blue-500 px-4 py-2 rounded-md hover:bg-blue-500 hover:text-white" href="{{ route('gallery.edit', $item->id) }}">Edit</a>
                                    <form class="" action="{{ route('gallery.destroy', $item->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button 
                                            class="text-red-500 border border-red-500 px-4 py-2 rounded-md hover:bg-red-500 hover:text-white" 
                                            type="submit">
                                            Delete
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
