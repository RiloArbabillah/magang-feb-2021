@extends('layout.default')
@section('content')
    <div class="flex flex-col h-screen bg-gray-300">
        <div class="flex flex-col align-middle justify-center items-center mt-32">
            <div class="text-7xl flex item-center justify-center mb-10 tracking-wider text-primary">
                Buat Gallery Baru
            </div>
            <form 
                enctype="multipart/form-data" 
                action="{{ route('gallery.store') }}" 
                method="post" 
                class="max-w-lg space-y-4 w-full border-2 border-blue-500 p-4 rounded-md"
            >
                @csrf
                <div class="w-full">
                    <label for="nama" class="w-full">Nama :</label>
                    <input type="text" name="nama" class="w-full px-4 py-2 rounded-md mt-1">
                </div>
                <div class="w-full">
                    <label for="file" class="w-full">File :</label>
                    <div class="w-full px-4 py-2 rounded-md mt-1 bg-white">
                        <input type="file" name="file" class="w-full">
                    </div>
                </div>
                <button type="submit" class="px-4 py-2 bg-blue-600 text-white hover:bg-blue-500 rounded-md">Simpan</button>
            </form>
        </div>
    </div>
@endsection