<?php

use Illuminate\Database\Seeder;
use App\Models\Link;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);

        $links = [
            [ 'nama' => 'Youtube', 'url' => 'https://www.youtube.com/' ],
            [ 'nama' => 'Facebook', 'url' => 'https://www.facebook.com/' ],
            [ 'nama' => 'Instagram', 'url' => 'https://www.instagram.com/' ],
        ];

        foreach ($links as $key => $value) {
            Link::create([
                'nama' => $value['nama'],
                'url' => $value['url'],
            ]);
        }
    }
}
