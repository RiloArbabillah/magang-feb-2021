<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Perusahaan;

class Link extends Model
{
    protected $table = 'links';

    protected $fillable = [
        'nama', 'url'
    ];

    public function perusahaan()
    {
        return $this->hasMany(Perusahaan::class, 'link_id');
    }
}
