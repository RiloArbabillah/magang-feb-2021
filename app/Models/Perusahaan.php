<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Link;

class Perusahaan extends Model
{
    protected $table = 'perusahaan';

    protected $guarded = [
        'id', 'created_at', 'updated_at'
    ];

    public function link()
    {
        return $this->belongsTo(Link::class, 'link_id');
    }
}
