<?php

namespace App\Http\Controllers\Link;

use App\Http\Controllers\Controller;
use App\Models\Link;
use Illuminate\Http\Request;

class LinkController extends Controller
{
    //
    public function index(Request $request)
    {
        $teks = 'Sebuah teks untuk dibaca';
        $links = Link::all();
        
        return view('link.link', compact('teks', 'links'));
    }

    public function create()
    {
        return view('link.linkForm');
    }

    public function store(Request $request)
    {
        $link = Link::create([
            'nama' => $request->nama,
            'url' => $request->url,
        ]);

        return redirect()->route('link.index');
    }

    public function edit(Link $link)
    {
        return view('link.linkFormEdit', compact('link'));
    }
    
    public function update(Link $link, Request $request)
    {
        $link->update([
            'nama' => $request->nama,
            'url' => $request->url,
        ]);
        return redirect()->route('link.index');
    }

    public function destroy(Link $link)
    {
        $link->delete();
        return back();
    }
}
