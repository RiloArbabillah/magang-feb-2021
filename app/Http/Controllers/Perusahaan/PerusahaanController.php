<?php

namespace App\Http\Controllers\Perusahaan;

use App\Http\Controllers\Controller;
use App\Models\Perusahaan;
use App\Models\Link;
use Illuminate\Http\Request;

class PerusahaanController extends Controller
{
    //
    public function index(Request $request)
    {
        $perusahaan = Perusahaan::all();
        
        return view('perusahaan.perusahaan', compact('perusahaan'));
    }

    public function create()
    {
        $link = Link::all();
        return view('perusahaan.perusahaanForm', compact('link'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => ['required'],
            'alamat' => ['required'],
            'email' => ['required'],
            'telp' => ['required', 'numeric'],
            'linkId' => ['required', 'numeric'],
        ]);

        $perusahaan = Perusahaan::create([
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'email' => $request->email,
            'telp' => $request->telp,
            'link_id' => $request->linkId,
        ]);

        return redirect()->route('perusahaan.index');
    }

    public function edit(Perusahaan $perusahaan)
    {
        $perusahaan = $perusahaan->with('link')->where('id', $perusahaan->id)->first();
        $link = Link::where('id', '<>', $perusahaan->link_id)->get();
        return view('perusahaan.perusahaanFormEdit', compact('perusahaan', 'link'));
    }
    
    public function update(Perusahaan $perusahaan, Request $request)
    {
        $request->validate([
            'nama' => ['required'],
            'alamat' => ['required'],
            'email' => ['required'],
            'telp' => ['required', 'numeric'],
            'linkId' => ['required', 'numeric'],
        ]);

        $perusahaan->update([
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'email' => $request->email,
            'telp' => $request->telp,
            'link_id' => $request->linkId,
        ]);
        
        return redirect()->route('perusahaan.index');
    }

    public function destroy(Perusahaan $perusahaan)
    {
        $perusahaan->delete();
        return back();
    }
}
