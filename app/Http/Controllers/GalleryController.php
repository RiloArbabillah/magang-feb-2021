<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Gallery;
use Illuminate\Support\Str;

class GalleryController extends Controller
{
    public function index()
    {
        $galleries = Gallery::all();
        return view('gallery.gallery', compact('galleries'));
    }
    
    public function create()
    {
        return view('gallery.galleryForm');
    }

    public function store(Request $request)
    {
        $request->validate([
            'file' => ['file', 'required'],
            'nama' => ['required'],
        ]);

        $file = $request->file;
        $nama = $request->nama;

        $mime = $file->getMimeType();
        $filename = $file->getClientOriginalName();
        $type = Str::is('image/*', $mime) ? 'image' : 'file';
        $path = 'galleries/';
        $file->storeAs('public/', $path . $filename);

        $meta = [
            'mime' => $mime,
            'name' => $filename,
            'type' => $type,
        ];

        $gallery = Gallery::create([
            'nama' => $nama,            
            'path' => $path . $filename,
            'meta' => $meta,
        ]);

        return redirect()->route('gallery.index');
    }

    public function edit(Gallery $gallery)
    {
    }

    public function update(Gallery $gallery)
    {
    }

    public function destroy(Gallery $gallery)
    {
        $path = storage_path('app/public/' . $gallery->path);
        @unlink($path);
        $gallery->delete();
        return back();
    }
}
