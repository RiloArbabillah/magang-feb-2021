module.exports = {
  purge: [
    './resources/**/*.blade.php',
    './resources/**/*.js',
    './resources/**/*.vue',
  ],
  darkMode: 'media', // or 'media' or 'class'
  theme: { 
    extend: {},
    // textColor: {
    //   // 'primary': '#3490dc',
    //   // 'secondary': '#ffed4a',
    //   // 'danger': '#e3342f',
    // }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
